package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.ticket.controller.TicketController;
import pl.polsl.domain.ticket.services.IGetTicket;
import pl.polsl.domain.ticket.services.ISaveTicket;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedTicketControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IGetTicket iGetTicket;

    @Mock
    private ISaveTicket iSaveTicket;

    @InjectMocks
    private TicketController ticketController;

    private String linkToPostGet = "/tickets";
    private String linkToPutDelete = "/ticket/1";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(ticketController).build();
    }

    @Test
    public void test_1_Post() {
        String postMessage = "{\"room\":{\"id\":1,\"inventory\":{\"id\":1,\"symbol\":\"test_this\",\"inventoryElementsList\":{\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}}},\"inventoryElement\":{\"id\":1,\"name\":\"lamp\",\"broken\":false},\"description\":\"opis\",\"comment\":\"\",\"priority\":5,\"done\":false,\"confirmed\":true}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {
        String postMessage = "{\"test_this\",\"inventoryElementsList\":{\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}}},\"inventoryElement\":{\"id\":1,\"name\":\"lamp\",\"broken\":false},\"description\":\"opis\",\"comment\":\"\",\"priority\":5,\"done\":false,\"confirmed\":true}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_2_Get() {
        try {
            mockMvc.perform(
                    MockMvcRequestBuilders.get(linkToPostGet)
            ).andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_Put() {
        String putMessage = "{\"room\":{\"id\":1,\"inventory\":{\"id\":1,\"symbol\":\"PRO\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}},\"inventoryElement\":{\"id\":1,\"name\":\"zmiany\",\"broken\":false},\"description\":\"zmiany\",\"comment\":\"zmiany\",\"priority\":5,\"notificationDate\":\"24.05.zmiany\",\"acceptationDate\":\"24.05.zmiany\",\"executionDate\":\"\",\"rate\":0,\"done\":false,\"confirmed\":true}";

        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_NegativePut() {
        String putMessage = "{inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_4_Delete() {
        try {
            mockMvc.perform(delete(linkToPutDelete))
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }
}

