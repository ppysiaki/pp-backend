package pl.polsl.infrastructure.adapters.inventoryElement;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToInventoryElement implements IMapTo<InventoryElement, InventoryElementEntity> {

    public InventoryElementEntity toEntity(InventoryElement dto, @Nullable InventoryElementEntity entity) {
        if (entity == null) {
            entity = new InventoryElementEntity();
            entity.setId(dto.getId());
        }

        entity.setName(dto.getName());
        entity.setIsBroken(dto.isBroken());
        entity.setInventoryId(dto.getInventoryId());

        return entity;
    }

    @Override
    public InventoryElement toDto(@Nullable InventoryElementEntity entity) {
        if (entity == null)
            return null;

        InventoryElement dto = new InventoryElement();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.linkToInventory(entity.getInventoryId());

        if (entity.isBroken()) {
            dto.markAsBroken();
        } else {
            dto.markAsNotBroken();
        }

        return dto;
    }

}
