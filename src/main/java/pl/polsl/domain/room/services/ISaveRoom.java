package pl.polsl.domain.room.services;

import pl.polsl.domain.room.Room;
import pl.polsl.domain.room.exception.NoSuchRoom;

public interface ISaveRoom {
    void save(Room inventory);

    void update(Integer id, Room room) throws NoSuchRoom;

    void delete(Integer id);
}
