package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.inventory.controller.InventoryController;
import pl.polsl.domain.inventory.services.IGetInventory;
import pl.polsl.domain.inventory.services.ISaveInventory;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedInventoryControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IGetInventory iGetInventory;

    @Mock
    private ISaveInventory iSaveInventory;

    @InjectMocks
    private InventoryController inventoryController;

    private String linkToPostGet = "/inventories";
    private String linkToPutDelete = "/inventory/1";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(inventoryController).build();
    }

    @Test
    public void test_1_Post() {
        try {
            String postMessage = "{\"symbol\":\"testowy_post\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {
        try {
            String postMessage = "{:[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_2_Get() {
        try {
            mockMvc.perform(
                    MockMvcRequestBuilders.get(linkToPostGet)
            ).andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_Put() {
        String putMessage = "{\"id\":1,\"symbol\":\"test_put\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_NegativePut() {
        String putMessage = "{inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_4_Delete() {
        try {
            mockMvc.perform(delete(linkToPutDelete))
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }
}

