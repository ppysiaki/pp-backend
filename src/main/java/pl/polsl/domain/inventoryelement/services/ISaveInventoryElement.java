package pl.polsl.domain.inventoryelement.services;

import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.inventoryelement.exception.NoSuchInventoryElement;

public interface ISaveInventoryElement {
    void save(InventoryElement inventory);

    void update(Integer id, InventoryElement inventory) throws NoSuchInventoryElement;

    void delete(Integer id);
}