package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.identity.controller.UserController;
import pl.polsl.domain.identity.service.IAuthenticationService;
import pl.polsl.domain.identity.service.IRoleService;
import pl.polsl.domain.identity.service.IUserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedUserControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IUserService iUserService;

    @Mock
    private IRoleService iRoleService;

    @Mock
    private IAuthenticationService iAuthenticationService;

    @InjectMocks
    private UserController userController;

    private String linkToPostGet = "/user";
    private String linkToPostGet2 = "/user/2/roles";
    private String linkToDeleteRole = "/user/2/roles/2";


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void test_1_Post() {
        String addAdminRoleMessage = "{\"user\":\"test2\",\"roleId\":2}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(addAdminRoleMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {
        String addAdminRoleMessage = "{\"roleId\":2}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(addAdminRoleMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_2_Get_User() {
        try {
            mockMvc.perform(
                    MockMvcRequestBuilders.get(linkToPostGet)
            ).andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_2_Get_UserRole() {
        try {
            mockMvc.perform(
                    MockMvcRequestBuilders.get(linkToPostGet2)
            ).andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_4_Delete() {
        try {
            mockMvc.perform(delete(linkToDeleteRole))
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }
}

