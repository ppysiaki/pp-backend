package pl.polsl.infrastructure.identity.utility;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.valueObject.Password;
import pl.polsl.domain.identity.valueObject.UserId;
import pl.polsl.domain.identity.valueObject.Username;
import pl.polsl.infrastructure.identity.entity.RoleEntity;
import pl.polsl.infrastructure.identity.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserConverter {

    private final RoleConverter roleConverter;

    public UserConverter(RoleConverter roleConverter) {
        this.roleConverter = roleConverter;
    }

    public User entityToDto(UserEntity userEntity) {
        List<Role> roles = new ArrayList<>();
        for (RoleEntity role : userEntity.getRoles()) {
            roles.add(roleConverter.entityToDto(role));
        }

        return new User(
                new UserId(userEntity.getId()),
                new Username(userEntity.getUsername()),
                new Password(userEntity.getPassword()),
                roles
        );
    }

    public UserEntity dtoToEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId().getId());
        userEntity.setPassword(user.getPassword().toString());
        userEntity.setPassword(user.getUsername().toString());

        return userEntity;
    }
}
