package pl.polsl.infrastructure.adapters.examples;

import javax.persistence.*;

@Entity
@Table(name = "example")
class ExampleEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String message;

    String getMessage() {
        return message;
    }

    void setMessage(String message) {
        this.message = message;
    }

    Integer getId() {
        return id;
    }

    void setId(Integer id) {
        this.id = id;
    }
}
