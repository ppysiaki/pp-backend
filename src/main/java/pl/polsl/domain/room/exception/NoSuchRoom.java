package pl.polsl.domain.room.exception;

public class NoSuchRoom extends Exception {
    public NoSuchRoom() {
        super("There is no such Room in given parameters");
    }
}
