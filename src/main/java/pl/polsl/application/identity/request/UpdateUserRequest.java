package pl.polsl.application.identity.request;

import java.util.List;

public class UpdateUserRequest {

    private final String username;
    private final List<RoleRequest> roles;

    public UpdateUserRequest(String username, List<RoleRequest> roles) {

        this.username = username;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleRequest> getRoles() {
        return roles;
    }
}
