package pl.polsl.domain.identity.command;

public class CreateUser {

    private String username;

    private String password;

    public CreateUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
