package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
import org.junit.jupiter.api.Test;
//import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.inventoryelement.controller.InventoryElementController;
import pl.polsl.domain.inventoryelement.services.IGetInventoryElement;
import pl.polsl.domain.inventoryelement.services.ISaveInventoryElement;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedInventoryElementControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IGetInventoryElement iGetInventoryElement;

    @Mock
    private ISaveInventoryElement iSaveInventoryElement;

    @InjectMocks
    private InventoryElementController inventoryElementController;

    private String linkToPostGet = "/inventory-elements";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(inventoryElementController).build();
    }

    @Test
    public void test_1_Post() {

        String postMessage = "{\"name\":\"test_post\",\"broken\":false}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {
        try {
            String postMessage = "{:[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }
}

