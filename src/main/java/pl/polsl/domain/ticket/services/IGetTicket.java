package pl.polsl.domain.ticket.services;

import pl.polsl.domain.ticket.Ticket;

import java.util.List;

public interface IGetTicket {
    Ticket byId(Integer id);

    List<Ticket> getAll();

    List<Ticket> findAllByRoomId(Integer id);

}
