package pl.polsl.infrastructure.identity.encoder;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class BCryptEncoder implements IEncoder {

    private final BCryptPasswordEncoder encoder;

    public BCryptEncoder() {
        this.encoder = new BCryptPasswordEncoder();
    }

    @Override
    public String encode(String text) {
        return encoder.encode(text);
    }

    public Boolean match(String rawOldPassword, String encodedRawPassword) {
        return this.encoder.matches(rawOldPassword, encodedRawPassword);
    }
}
