package pl.polsl.domain.inventory.exception;

public class NoSuchInventory extends Exception {
    public NoSuchInventory() {
        super("There is no such Inventory in given parameters");
    }
}
