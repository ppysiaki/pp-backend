package pl.polsl.infrastructure.adapters.room;

import org.springframework.stereotype.Component;
import pl.polsl.domain.room.Room;
import pl.polsl.domain.room.exception.NoSuchRoom;
import pl.polsl.domain.room.services.ISaveRoom;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveRoom implements ISaveRoom {

    private RoomRepository roomRepository;

    private IMapTo<Room, RoomEntity> iMapTo;

    public SaveRoom(RoomRepository roomRepository, IMapTo<Room, RoomEntity> iMapTo) {
        this.roomRepository = roomRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(Room room) {
        this.roomRepository.save(iMapTo.toEntity(room, null));
    }

    @Override
    public void update(Integer id, Room inventory) throws NoSuchRoom {
        Optional<RoomEntity> entity = this.roomRepository.findById(id);

        entity.orElseThrow(NoSuchRoom::new);

        this.roomRepository.save(iMapTo.toEntity(inventory, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.roomRepository.deleteById(id);
    }
}
