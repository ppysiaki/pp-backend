package pl.polsl.application.inventory.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.application.ResponseError;
import pl.polsl.domain.inventory.Inventory;
import pl.polsl.domain.inventory.exception.InventoryIsInUse;
import pl.polsl.domain.inventory.exception.NoSuchInventory;
import pl.polsl.domain.inventory.services.IGetInventory;
import pl.polsl.domain.inventory.services.ISaveInventory;
import pl.polsl.domain.inventoryelement.InventoryElement;

import java.util.List;
import java.util.Optional;

@RestController
public class InventoryController {
    private IGetInventory iGetInventory;
    private ISaveInventory iSaveInventory;

    public InventoryController(IGetInventory iGetInventory, ISaveInventory iSaveInventory) {
        this.iGetInventory = iGetInventory;
        this.iSaveInventory = iSaveInventory;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventories")
    public List<Inventory> getAll() {
        return iGetInventory.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventory/{id}")
    public Inventory byId(@PathVariable Integer id) {
        return iGetInventory.byId(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventory/{id}/inventory-elements")
    public List<InventoryElement> getAll(@PathVariable Integer id) {
        return iGetInventory.byId(id).getInventoryElementsList().getInventoryElementList();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventory/{id}/inventory-elements/{elementId}")
    public Optional<InventoryElement> byId(@PathVariable Integer id, @PathVariable Integer elementId) {
        return iGetInventory.byId(id).getInventoryElementsList().byId(elementId);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/inventory/{id}")
    public ResponseEntity update(@PathVariable Integer id, @RequestBody Inventory inventory) {
        try {
            iSaveInventory.update(id, inventory);
        } catch (NoSuchInventory noSuchExample) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/inventories")
    ResponseEntity add(@RequestBody Inventory inventory) {
        iSaveInventory.save(inventory);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/inventory/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            iSaveInventory.delete(id);
        } catch (InventoryIsInUse exception) {
            return new ResponseEntity<>(ResponseError.fromException(exception), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
