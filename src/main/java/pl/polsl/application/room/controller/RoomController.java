package pl.polsl.application.room.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.room.Room;
import pl.polsl.domain.room.exception.NoSuchRoom;
import pl.polsl.domain.room.services.IGetRoom;
import pl.polsl.domain.room.services.ISaveRoom;

import java.util.List;

@RestController
public class RoomController {
    private final IGetRoom iGetRoom;
    private final ISaveRoom iSaveRoom;

    public RoomController(IGetRoom iGetRoom, ISaveRoom iSaveRoom) {
        this.iGetRoom = iGetRoom;
        this.iSaveRoom = iSaveRoom;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/rooms")
    public List<Room> getAll() {
        return iGetRoom.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/room/{id}")
    public Room byId(@PathVariable Integer id) {
        return iGetRoom.byId(id);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/room/{id}")
    public ResponseEntity update(@PathVariable Integer id, @RequestBody Room room) {
        try {
            iSaveRoom.update(id, room);
        } catch (NoSuchRoom noSuchRoom) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/rooms")
    ResponseEntity create(@RequestBody Room inventory) {
        iSaveRoom.save(inventory);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/room/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        iSaveRoom.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }
}
