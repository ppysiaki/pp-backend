package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
import org.junit.jupiter.api.Test;
//import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.room.controller.RoomController;
import pl.polsl.domain.room.services.IGetRoom;
import pl.polsl.domain.room.services.ISaveRoom;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedRoomControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IGetRoom iGetRoom;

    @Mock
    private ISaveRoom iSaveRoom;

    @InjectMocks
    private RoomController roomController;

    private String linkToPostGet = "/rooms";
    private String linkToPutDelete = "/room/1";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(roomController).build();
    }

    @Test
    public void test_1_Post() {

        String postMessage = "{\"inventory\":{\"id\":1,\"symbol\":\"testowy_room\",\"inventoryElementsList\":{\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp_test\",\"broken\":false},{\"id\":2,\"name\":\"table_test\",\"broken\":false},{\"id\":3,\"name\":\"bed_test\",\"broken\":true}]}}}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {

        String postMessage = "{\"inventoryElementsList\":{\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp_test\",\"broken\":false},{\"id\":2,\"name\":\"table_test\",\"broken\":false},{\"id\":3,\"name\":\"bed_test\",\"broken\":true}]}}}";

        try {
            mockMvc.perform(post(linkToPostGet)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(postMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_2_Get() {
        try {
            mockMvc.perform(
                    MockMvcRequestBuilders.get(linkToPostGet)
            ).andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_Put() {
        String putMessage = "{\"inventory\":{\"id\":2,\"symbol\":\"puted\",\"inventoryElementList\":null}}";

        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_3_NegativePut() {
        String putMessage = "{inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        try {
            mockMvc.perform(put(linkToPutDelete)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(putMessage))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }

    @Test
    public void test_4_Delete() {
        try {
            mockMvc.perform(delete(linkToPutDelete))
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }
}

