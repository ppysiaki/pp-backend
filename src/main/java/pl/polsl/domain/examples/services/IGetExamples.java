package pl.polsl.domain.examples.services;

import pl.polsl.domain.examples.Example;

import java.util.List;

public interface IGetExamples {
    Example byId(Integer id);

    List<Example> getAll();
}
