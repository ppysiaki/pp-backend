package pl.polsl.infrastructure.collection;

public interface IMapTo<Dto, Entity> {

    Entity toEntity(Dto dto, Entity entity);

    Dto toDto(Entity entity);
}
