package pl.polsl.infrastructure.adapters.examples;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.examples.Example;
import pl.polsl.domain.examples.exception.NoSuchExample;
import pl.polsl.domain.examples.services.ISaveExamples;

import java.util.Optional;

@Component
public class SaveExamples implements ISaveExamples {

    private ExampleRepository exampleRepository;

    public SaveExamples(ExampleRepository exampleRepository) {
        this.exampleRepository = exampleRepository;
    }

    @Override
    public void save(Example example) {
        exampleRepository.save(toEntity(example, null));
    }

    @Override
    public void update(Integer id, Example example) throws NoSuchExample {
        Optional<ExampleEntity> entity = exampleRepository.findById(id);

        entity.orElseThrow(NoSuchExample::new);

        exampleRepository.save(toEntity(example, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        exampleRepository.deleteById(id);
    }

    private ExampleEntity toEntity(Example example, @Nullable ExampleEntity entity) {
        if (entity == null) {
            entity = new ExampleEntity();
            entity.setId(example.getId());
        }

        entity.setMessage(example.getMessage());

        return entity;
    }
}
