package pl.polsl.infrastructure.adapters.ticket;

import org.springframework.stereotype.Component;
import pl.polsl.domain.ticket.Ticket;
import pl.polsl.domain.ticket.services.IGetTicket;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetTicket implements IGetTicket {

    private TicketRepository ticketRepository;

    private IMapTo<Ticket, TicketEntity> iMapTo;

    public GetTicket(TicketRepository ticketRepository, IMapTo<Ticket, TicketEntity> iMapTo) {
        this.ticketRepository = ticketRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public Ticket byId(Integer id) {
        TicketEntity entity = this.ticketRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Ticket> getAll() {
        Iterable<TicketEntity> inventoryElementEntityList = this.ticketRepository.findAll();
        List<Ticket> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Ticket> findAllByRoomId(Integer id) {
        Iterable<TicketEntity> inventoryElementEntityList = this.ticketRepository.findAllByRoomId(id);
        List<Ticket> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}
