package pl.polsl.application.ticket.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.ticket.Ticket;
import pl.polsl.domain.ticket.exception.NoSuchTicket;
import pl.polsl.domain.ticket.services.IGetTicket;
import pl.polsl.domain.ticket.services.ISaveTicket;

import java.util.List;

@RestController
public class TicketController {
    private IGetTicket iGetTicket;
    private ISaveTicket iSaveTicket;

    public TicketController(IGetTicket iGetTicket, ISaveTicket iSaveTicket) {
        this.iGetTicket = iGetTicket;
        this.iSaveTicket = iSaveTicket;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/tickets")
    public List<Ticket> getAll() {
        return iGetTicket.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/ticket/{id}")
    public Ticket byId(@PathVariable Integer id) {
        return iGetTicket.byId(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/ticket/{id}/inventory-elements")
    public InventoryElement getInventoryElement(@PathVariable Integer id) {
        return iGetTicket.byId(id).getInventoryElement();
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/ticket/{id}")
    public ResponseEntity update(@PathVariable Integer id, @RequestBody Ticket ticket) {
        try {
            iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/tickets")
    ResponseEntity create(@RequestBody Ticket ticket) {
        iSaveTicket.save(ticket);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/ticket/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        iSaveTicket.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket/{id}/confirm")
    public ResponseEntity confirmTicket(@PathVariable Integer id) {
        Ticket ticket = this.iGetTicket.byId(id);
        ticket.markAsConfirmed();

        try {
            this.iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket/{id}/dismiss")
    public ResponseEntity dismissTicket(@PathVariable Integer id) {
        Ticket ticket = this.iGetTicket.byId(id);
        ticket.markAsNotConfirmed();

        try {
            this.iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket/{id}/done")
    public ResponseEntity markAsDone(@PathVariable Integer id) {
        Ticket ticket = this.iGetTicket.byId(id);
        ticket.markAsDone();

        try {
            this.iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket/{id}/not-done")
    public ResponseEntity markAsNotDone(@PathVariable Integer id) {
        Ticket ticket = this.iGetTicket.byId(id);
        ticket.markAsNotDone();

        try {
            this.iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ticket/{id}/rate/{rateValue}")
    public ResponseEntity addRate(@PathVariable Integer id, @PathVariable Integer rateValue) {
        Ticket ticket = this.iGetTicket.byId(id);
        ticket.addRate(rateValue);

        try {
            this.iSaveTicket.update(id, ticket);
        } catch (NoSuchTicket e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
