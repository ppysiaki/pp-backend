package pl.polsl.application.identity.reponse;

public class RoleResponse {

    private final int id;
    private final String name;

    public RoleResponse(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
