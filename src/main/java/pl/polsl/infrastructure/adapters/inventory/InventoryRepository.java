package pl.polsl.infrastructure.adapters.inventory;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface InventoryRepository extends CrudRepository<InventoryEntity, Integer> {
    Optional<InventoryEntity> findById(Integer id);
}
