package pl.polsl.domain.identity.dto;

import pl.polsl.domain.identity.valueObject.Password;
import pl.polsl.domain.identity.valueObject.UserId;
import pl.polsl.domain.identity.valueObject.Username;

import java.util.List;
import java.util.Optional;

public class User {

    private final UserId id;
    private final Username username;
    private final Password password;
    private final List<Role> roles;

    public User(UserId id, Username username, Password password, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public UserId getId() {
        return id;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public Optional<Role> getRoleById(Integer id) {
        return roles.stream().filter(predicate -> predicate.getRoleId().getId().equals(id)).findFirst();
    }

    public void deleteRoleById(Integer id) {
        roles.stream().filter(predicate -> predicate.getRoleId().getId().equals(id)).findFirst().ifPresent(roles::remove);
    }

}
