package pl.polsl.domain.room.services;

import pl.polsl.domain.room.Room;

import java.util.List;

public interface IGetRoom {
    Room byId(Integer id);

    List<Room> getAll();
}
