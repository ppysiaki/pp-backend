package pl.polsl.domain.inventory.services;

import pl.polsl.domain.inventory.Inventory;
import pl.polsl.domain.inventory.exception.InventoryIsInUse;
import pl.polsl.domain.inventory.exception.NoSuchInventory;

public interface ISaveInventory {
    void save(Inventory inventory);

    void update(Integer id, Inventory inventory) throws NoSuchInventory;

    void delete(Integer id) throws InventoryIsInUse;
}
