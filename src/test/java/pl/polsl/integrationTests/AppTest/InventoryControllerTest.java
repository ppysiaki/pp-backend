package pl.polsl.integrationTests.AppTest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InventoryControllerTest {

    @LocalServerPort
    private int port;

    private String linkToPostGet = "/inventories/";
    private String linkToPutDel = "/inventory/";

    private String postMessage = "{\"symbol\":\"testowy_post\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
    private String postNameToTest = "testowy_post";

    private int start_json_len;
    private int json_len_after_post;
    private static String id_to_del;

    Helpers helper = new Helpers();

    @Test
    public void testGet() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(helper.getUrl() + port + linkToPostGet);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);
        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'PRO'",
                getContent, containsString("PRO"));

        start_json_len = getContent.length();
    }

    @Test
    public void testPost() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpPost postRequest = new HttpPost(helper.getUrl() + port + linkToPostGet);
        StringEntity entity = new StringEntity(postMessage);

        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        postRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest.setEntity(entity);
        HttpResponse postResponse = HttpClientBuilder.create().build().execute(postRequest);

        assertEquals(200, postResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after POST request");

        String getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertThat("GET response contains POSTed message",
                getContent, containsString(postNameToTest));

        json_len_after_post = getContent.length();

        assertTrue("POSTed message added some stuff to DB",
                start_json_len < json_len_after_post);

        Pattern p = Pattern.compile("(\"id\":)\\d+(,\"symbol\":\"testowy_post\")");
        Matcher m = p.matcher(getContent);

        String match = null;

        while (m.find())
            match = m.group();

        id_to_del = match.replaceAll("\\D+", "");
    }

    @Test
    public void testTheDelete() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpDelete delRequest = new HttpDelete(helper.getUrl() + port + linkToPutDel + id_to_del);
        delRequest.setHeader("Accept", "application/json");
        delRequest.setHeader("Content-type", "application/json");
        delRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse delResponse = HttpClientBuilder.create().build().execute(delRequest);

        assertEquals(200, delResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after DELETE request");

        String getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertFalse("GET response confirms that record were deleted",
                getContent.contains(postNameToTest));
    }

    @Test
    public void testThePut() throws IOException {

        String id_to_put = "1";
        String putMessage = "{\"id\":1,\"symbol\":\"test_put\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        String revertChanges = "{\"id\":1,\"symbol\":\"PRO\",\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp\",\"broken\":false},{\"id\":2,\"name\":\"table\",\"broken\":false},{\"id\":3,\"name\":\"bed\",\"broken\":true}]}";
        String putName = "test_put";

        String authToken = "Bearer " + helper.createToken();
        StringEntity entity = new StringEntity(putMessage);

        HttpPut putRequest = new HttpPut(helper.getUrl() + port + linkToPutDel + id_to_put);
        putRequest.setHeader("Accept", "application/json");
        putRequest.setHeader("Content-type", "application/json");
        putRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        putRequest.setEntity(entity);
        HttpResponse putResponse = HttpClientBuilder.create().build().execute(putRequest);

        assertEquals(200, putResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after PUT request");

        String getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertThat("GET response contains PUTed message",
                getContent, containsString(putName));

        // Revert Changes:
        entity = new StringEntity(revertChanges);
        putRequest = new HttpPut(helper.getUrl() + port + linkToPutDel + id_to_put);
        putRequest.setHeader("Accept", "application/json");
        putRequest.setHeader("Content-type", "application/json");
        putRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        putRequest.setEntity(entity);
        HttpClientBuilder.create().build().execute(putRequest);
    }
}
