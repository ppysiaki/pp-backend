package pl.polsl.application.inventoryelement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.inventoryelement.exception.NoSuchInventoryElement;
import pl.polsl.domain.inventoryelement.services.IGetInventoryElement;
import pl.polsl.domain.inventoryelement.services.ISaveInventoryElement;

import java.util.List;

@RestController
public class InventoryElementController {

    private final IGetInventoryElement iGetInventoryElement;
    private final ISaveInventoryElement iSaveInventoryElement;

    public InventoryElementController(IGetInventoryElement iGetInventoryElement, ISaveInventoryElement iSaveInventoryElement) {
        this.iGetInventoryElement = iGetInventoryElement;
        this.iSaveInventoryElement = iSaveInventoryElement;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventory-elements")
    public ResponseEntity<List<InventoryElement>> get() {
        List<InventoryElement> list = iGetInventoryElement.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inventory-elements/unassigned")
    public ResponseEntity<List<InventoryElement>> getUnassigned() {
        List<InventoryElement> list = iGetInventoryElement.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/inventory-element/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody InventoryElement inventoryElement) {
        try {
            iSaveInventoryElement.update(id, inventoryElement);
        } catch (NoSuchInventoryElement noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/inventory-elements")
    ResponseEntity<HttpStatus> add(@RequestBody InventoryElement inventoryElement) {
        iSaveInventoryElement.save(inventoryElement);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/inventory-element/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer id) {
        iSaveInventoryElement.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
