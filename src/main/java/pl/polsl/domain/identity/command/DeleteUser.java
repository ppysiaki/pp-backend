package pl.polsl.domain.identity.command;

import pl.polsl.domain.identity.valueObject.UserId;

public class DeleteUser {

    private final UserId userId;

    public DeleteUser(UserId userId) {

        this.userId = userId;
    }

    public UserId getUserId() {
        return userId;
    }
}
