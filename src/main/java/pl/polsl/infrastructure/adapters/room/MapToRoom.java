package pl.polsl.infrastructure.adapters.room;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventory.services.IGetInventory;
import pl.polsl.domain.room.Room;
import pl.polsl.infrastructure.adapters.inventory.InventoryRepository;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToRoom implements IMapTo<Room, RoomEntity> {

    private final InventoryRepository inventoryRepository;

    private final IGetInventory iGetInventory;

    public MapToRoom(InventoryRepository inventoryRepository, IGetInventory iGetInventory) {
        this.inventoryRepository = inventoryRepository;
        this.iGetInventory = iGetInventory;
    }

    @Override
    public RoomEntity toEntity(Room dto, RoomEntity entity) {
        if (null == entity) {
            entity = new RoomEntity();
            entity.setId(dto.getId());
        }
        if (dto.getInventory() != null) {
            entity.setInventory(inventoryRepository.findById(dto.getInventory().getId()).orElse(null));
        }
        entity.setLabel(dto.getLabel());

        return entity;
    }

    @Override
    public Room toDto(RoomEntity entity) {
        Room dto = new Room();
        dto.setId(entity.getId());
        if (entity.getInventory() != null) {
            dto.setInventory(iGetInventory.byId(entity.getInventory().getId()));
        }
        dto.setLabel(entity.getLabel());

        return dto;
    }
}
