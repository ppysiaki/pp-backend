package pl.polsl.infrastructure.adapters.inventoryElement;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface InventoryElementRepository extends CrudRepository<InventoryElementEntity, Integer> {
    Optional<InventoryElementEntity> findById(Integer id);

    List<InventoryElementEntity> findAllByInventoryId(Integer inventoryId);

    List<InventoryElementEntity> findAllByInventoryIdIsNull();
}
