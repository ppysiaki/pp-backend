package pl.polsl.domain.inventoryelement.exception;

public class NoSuchInventoryElement extends Exception {
    public NoSuchInventoryElement() {
        super("There is no such InventoryElement in given parameters");
    }
}
