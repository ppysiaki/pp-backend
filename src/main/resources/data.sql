INSERT INTO inventory VALUES (1, 'Inwentarz 1');
INSERT INTO inventory VALUES (2, 'Inwentarz 2');
INSERT INTO inventory VALUES (3, 'Inventarz 3');
INSERT INTO inventory VALUES (4, 'Inventarz 4');
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (1, 'Lamp', false, 1);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (2, 'Table', false, 1);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (3, 'Bed', false, 1);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (4, 'Tv', false, 2);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (5, 'Lamp', false, null);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (6, 'Table', false, null);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (7, 'Bed', false, null);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (8, 'Tv', false, null);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (9, 'Lamp', false, 3);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (10, 'Table', false, 3);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (11, 'Bed', false, 3);
INSERT INTO inventory_element(id, name, is_broken, inventory_id) VALUES (12, 'Tv', true, 3);


INSERT INTO room VALUES
(1,	101,1),
(2,	102,2),
(3,	103,3),
(4,	104,4),
(5,	105,null),
(6,	106,null),
(7,	107,null),
(8,	108,null),
(9,	109,null),
(10, 110, null),
(11, 111, null),
(12, 112, null),
(13, 113, null),
(14, 114, null),
(15, 115, null),
(16, 201, null),
(17, 202, null),
(18, 203, null),
(19, 204, null),
(20, 205, null),
(21, 206, null),
(22, 207, null),
(23, 208, null),
(24, 209, null),
(25, 210, null),
(26, 211, null),
(27, 212, null),
(28, 213, null),
(29, 214, null),
(30, 215, null);


INSERT INTO ticket (id, room_id, inventory_element_id, description, is_confirmed, comment, is_done, priority, notification_date, acceptation_date, execution_date, rate)
VALUES
(1, 1, 1, 'Proszę o wymianę żarówki w lampie', 0, '', 0, 5, '24.05.2020', '', '', 0),
(2, 3, 12, 'Obraz w TV się nie wyświetla', 1, 'Noga wyłamana ze stołu i wbita w telewizor, klient obciążony kosztami wymiany urządzenia', 1, 1, '22.05.2020', '23.05.2020', '24.05.2020', 5);


INSERT INTO role VALUES (1, 'ROLE_user');
INSERT INTO role VALUES (2, 'ROLE_admin');
INSERT INTO `user` VALUES (1, '$2a$10$45rnq06rSZnIgHuskXVMS.DelWmAZSGaba8vQ88rKCuHisPN4efMG', 'test');
INSERT INTO `user` VALUES (2, '$2a$10$45rnq06rSZnIgHuskXVMS.DelWmAZSGaba8vQ88rKCuHisPN4efMG', 'test2');
INSERT INTO `user_roles` VALUES (1, 1);
INSERT INTO `user_roles` VALUES (1, 2);
INSERT INTO `user_roles` VALUES (2, 1);
