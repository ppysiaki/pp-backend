package pl.polsl.integrationTests.MockedTests;

import org.junit.Before;
import org.junit.jupiter.api.Test;
//import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.polsl.application.identity.controller.MeController;
import pl.polsl.domain.identity.service.IUserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class MockedMeControllerTest {

//    To run tests from "PLAY BUTTON" please change
//    import org.junit.jupiter.api.Test;
//    to
//    import org.junit.Test;
//    Jupiter.api is used to run tests through maven.

    private MockMvc mockMvc;

    @Mock
    private IUserService iUserService;

    @InjectMocks
    private MeController meController;

    private String linkToPost = "/me/changepassword";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(meController).build();
    }

    @Test
    public void test_1_Post() {

        String passChanger = "{\"oldPassword\":\"test\",\"newPassword\":\"xxx\",\"user\":\"test\"}";

        try {
            mockMvc.perform(post(linkToPost)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(passChanger))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {}
    }

    @Test
    public void test_1_NegativePost() {

        String passChanger = "{\"newPassword\":\"xxx\",\"user\":\"test\"}";

        try {
            mockMvc.perform(post(linkToPost)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(passChanger))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        } catch (Exception e) {}
    }
}

