package pl.polsl.infrastructure.identity.repository;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.identity.entity.UserEntity;

import java.util.List;

public interface IUserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findOneByUsername(String username);

    UserEntity getById(Integer id);

    List<UserEntity> findAllByUsername(String username);

}
