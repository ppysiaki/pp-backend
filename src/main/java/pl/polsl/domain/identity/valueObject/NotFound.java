package pl.polsl.domain.identity.valueObject;

public class NotFound implements IResult {

    private final String message;

    public NotFound(String message) {

        this.message = message;
    }

    @Override
    public Integer getResultCode() {
        return 404;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
