package pl.polsl.infrastructure.adapters.ticket;

import pl.polsl.infrastructure.adapters.inventoryElement.InventoryElementEntity;
import pl.polsl.infrastructure.adapters.room.RoomEntity;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
public class TicketEntity {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private RoomEntity room;

    @ManyToOne
    private InventoryElementEntity inventoryElement;

    @Column(name = "description")
    private String description;

    @Column(name = "is_done")
    private boolean isDone;

    @Column(name = "comment")
    private String comment;

    @Column(name = "is_confirmed")
    private boolean isConfirmed;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "notification_date")
    private String notificationDate;

    @Column(name = "acceptation_date")
    private String acceptationDate;

    @Column(name = "execution_date")
    private String executionDate;

    @Column(name = "rate")
    private Integer rate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public void assignRoom(RoomEntity room) {
        this.room = room;
    }

    public InventoryElementEntity getInventoryElementEntity() {
        return inventoryElement;
    }

    public void setInventoryElementEntity(InventoryElementEntity inventoryElement) {
        this.inventoryElement = inventoryElement;
    }

    public String getDescription() {
        return description;
    }

    public void addDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setIsDone(boolean done) {
        isDone = done;
    }

    public String getComment() {
        return comment;
    }

    public void addComment(String comment) {
        this.comment = comment;
    }

    public InventoryElementEntity getInventoryElement() {
        return inventoryElement;
    }

    public void setInventoryElement(InventoryElementEntity inventoryElement) {
        this.inventoryElement = inventoryElement;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void addNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getAcceptationDate() {
        return acceptationDate;
    }

    public void addAcceptationDate(String acceptationDate) {
        this.acceptationDate = acceptationDate;
    }

    public String getExecutionDate() {
        return executionDate;
    }

    public void addExecutionDate(String executionDate) {
        this.executionDate = executionDate;
    }

    public Integer getRate() {
        return rate;
    }

    public void addRate(Integer rate) {
        this.rate = rate;
    }
}
