package pl.polsl.infrastructure.adapters.room;

import org.springframework.stereotype.Component;
import pl.polsl.domain.room.Room;
import pl.polsl.domain.room.services.IGetRoom;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetRoom implements IGetRoom {

    private RoomRepository roomRepository;

    private IMapTo<Room, RoomEntity> iMapTo;

    public GetRoom(RoomRepository roomRepository, IMapTo<Room, RoomEntity> iMapTo) {
        this.roomRepository = roomRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public Room byId(Integer id) {
        RoomEntity entity = this.roomRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Room> getAll() {
        Iterable<RoomEntity> inventoryElementEntityList = this.roomRepository.findAll();
        List<Room> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}
