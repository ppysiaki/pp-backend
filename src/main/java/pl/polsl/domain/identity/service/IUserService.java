package pl.polsl.domain.identity.service;

import pl.polsl.domain.identity.command.*;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.valueObject.IResult;
import pl.polsl.infrastructure.identity.exception.UserNotFoundException;
import pl.polsl.infrastructure.identity.exception.ValidationException;

import java.util.List;

public interface IUserService {
    void createUser(CreateUser command) throws ValidationException;

    User byId(Integer id);

    User findOneByUsername(String username);

    List<User> getAll();

    IResult assignRoleToUser(AssignRoleToUser data);

    void deleteUserRole(Integer userId, Integer roleId) throws UserNotFoundException;

    void changeUserPassword(ChangePassword command);

    void deleteUser(DeleteUser command) throws UserNotFoundException;

    void update(UpdateUser command) throws UserNotFoundException;
}
