package pl.polsl.integrationTests.AppTest;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InventoryElementControllerTest {

    @LocalServerPort
    private int port;

    private String link = "/inventory-elements/";
    private String postMessage = "{\"name\":\"test_post\",\"broken\":false}";
    Helpers helper = new Helpers();

    @Test
    public void testPost() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpPost postRequest = new HttpPost(helper.getUrl() + port + link);
        StringEntity entity = new StringEntity(postMessage);

        postRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        postRequest.setEntity(entity);

        HttpResponse postResponse = HttpClientBuilder.create().build().execute(postRequest);

        assertEquals(200, postResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after POST request");
    }
}
