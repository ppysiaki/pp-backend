package pl.polsl.infrastructure.adapters.ticket;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends CrudRepository<TicketEntity, Integer> {
    Optional<TicketEntity> findById(Integer id);

    List<TicketEntity> findAllByRoomId(Integer roomId);
}
