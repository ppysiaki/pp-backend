package pl.polsl.infrastructure.identity.validation;

public class ValidationResult {

    private final int status;

    private final String message;

    public ValidationResult(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
