package pl.polsl.infrastructure.adapters.ticket;

import org.springframework.stereotype.Component;
import pl.polsl.domain.ticket.Ticket;
import pl.polsl.domain.ticket.exception.NoSuchTicket;
import pl.polsl.domain.ticket.services.ISaveTicket;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveTicket implements ISaveTicket {

    private TicketRepository ticketRepository;
    private IMapTo<Ticket, TicketEntity> iMapTo;

    public SaveTicket(TicketRepository ticketRepository, IMapTo<Ticket, TicketEntity> iMapTo) {
        this.ticketRepository = ticketRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public void save(Ticket ticket) {
        this.ticketRepository.save(iMapTo.toEntity(ticket, null));
    }

    @Override
    public void update(Integer id, Ticket ticket) throws NoSuchTicket {
        Optional<TicketEntity> entity = this.ticketRepository.findById(id);

        entity.orElseThrow(NoSuchTicket::new);

        this.ticketRepository.save(iMapTo.toEntity(ticket, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.ticketRepository.deleteById(id);
    }
}
