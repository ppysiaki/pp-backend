package pl.polsl.domain.identity.command;

import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.UserId;

import java.util.List;

public class UpdateUser {

    private final UserId userId;
    private final String username;
    private final List<RoleId> roleIds;

    public UpdateUser(UserId userId, String username, List<RoleId> roleIds) {
        this.userId = userId;
        this.username = username;
        this.roleIds = roleIds;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleId> getRoleIds() {
        return roleIds;
    }

    public UserId getUserId() {
        return userId;
    }
}
