package pl.polsl.domain.ticket.services;

import pl.polsl.domain.ticket.Ticket;
import pl.polsl.domain.ticket.exception.NoSuchTicket;

public interface ISaveTicket {
    void save(Ticket ticket);

    void update(Integer id, Ticket ticket) throws NoSuchTicket;

    void delete(Integer id);
}
