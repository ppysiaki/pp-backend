package pl.polsl.integrationTests.AppTest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertFalse;


@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    Helpers helper = new Helpers();

    private String userUri = helper.getUrl() + helper.getPort8080() + "/user";
    private String user1RolesUri = helper.getUrl() + helper.getPort8080() + "/user/1/roles";
    private String user2RolesUri = helper.getUrl() + helper.getPort8080() + "/user/2/roles";
    private String userRoleUri = helper.getUrl() + helper.getPort8080() + "/user/1/roles/1";
    private String user2Uri = helper.getUrl() + helper.getPort8080() + "/user/2";
    private String user2AddRoleUri = helper.getUrl() + helper.getPort8080() + "/user/2/role";
    private String user2DeleteRoleUri = helper.getUrl() + helper.getPort8080() + "/user/2/roles/2";
    private String addAdminRoleMessage = "{\"user\":\"test2\",\"roleId\":2}";


    @Test
    public void testGetUser() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(userUri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'ROLE_user'",
                getContent, containsString("ROLE_user"));
    }

    @Test
    public void testGetUserRoles() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(user1RolesUri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'ROLE_user'",
                getContent, containsString("ROLE_user"));
    }

    @Test
    public void testGetUserRole() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(userRoleUri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'ROLE_user'",
                getContent, containsString("ROLE_user"));
    }

    @Test
    public void testGetUser2() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(user2Uri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'test2'",
                getContent, containsString("test2"));

        assertThat("GET response contains 'ROLE_user'",
                getContent, containsString("ROLE_user"));

        assertFalse("GET response doesn't contains 'ROLE_admin'",
                getContent.contains("ROLE_admin"));
    }

    @Test
    public void testAddAdminRoleToUser2() throws IOException {

        String authToken = "Bearer " + helper.createToken();
        StringEntity entity = new StringEntity(addAdminRoleMessage);

        HttpPost postRequest = new HttpPost(user2AddRoleUri);
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        postRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest.setEntity(entity);
        HttpResponse postResponse = HttpClientBuilder.create().build().execute(postRequest);

        HttpEntity postEntity = postResponse.getEntity();
        String postContent = EntityUtils.toString(postEntity, "UTF-8");

        assertThat("POST response contains 'Success'",
                postContent, containsString("Success"));


        HttpGet get = new HttpGet(user2RolesUri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'ROLE_user'",
                getContent, containsString("ROLE_user"));

        assertThat("GET response contains 'ROLE_admin'",
                getContent, containsString("ROLE_admin"));
    }

    @Test
    public void testDeleteAdminRoleFromUser2() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpDelete delRequest = new HttpDelete(user2DeleteRoleUri);
        delRequest.setHeader("Accept", "application/json");
        delRequest.setHeader("Content-type", "application/json");
        delRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse delResponse = HttpClientBuilder.create().build().execute(delRequest);

        assertEquals(200, delResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after DELETE request");

        String getContent = helper.getSomeContent(user2DeleteRoleUri);

        Assert.assertFalse("GET response confirms that record were deleted",
                getContent.contains("ROLE_ADMIN"));
    }
}
