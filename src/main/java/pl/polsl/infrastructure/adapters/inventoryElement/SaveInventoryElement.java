package pl.polsl.infrastructure.adapters.inventoryElement;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.inventoryelement.exception.NoSuchInventoryElement;
import pl.polsl.domain.inventoryelement.services.ISaveInventoryElement;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveInventoryElement implements ISaveInventoryElement {

    private InventoryElementRepository inventoryElementRepository;
    private IMapTo<InventoryElement, InventoryElementEntity> iMapTo;

    public SaveInventoryElement(InventoryElementRepository inventoryElementRepository, IMapTo<InventoryElement, InventoryElementEntity> iMapTo) {
        this.inventoryElementRepository = inventoryElementRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(InventoryElement inventory) {
        inventoryElementRepository.save(iMapTo.toEntity(inventory, null));
    }

    @Override
    public void update(Integer id, InventoryElement inventory) throws NoSuchInventoryElement {
        Optional<InventoryElementEntity> entity = this.inventoryElementRepository.findById(id);

        entity.orElseThrow(NoSuchInventoryElement::new);

        this.inventoryElementRepository.save(iMapTo.toEntity(inventory, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.inventoryElementRepository.deleteById(id);
    }


}
