package pl.polsl.application.identity.reponse;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDtoToResponse {
    private final RoleDtoConverter roleConverter;

    public UserDtoToResponse(RoleDtoConverter roleConverter) {
        this.roleConverter = roleConverter;
    }

    public UserResponse dtoToEntity(User user) {
        List<RoleResponse> roles = new ArrayList<>();
        user.getRoles().forEach((role) ->
            roles.add(roleConverter.dtoToResponse(role))
        );

        return new UserResponse(
                user.getId().getId(),
                user.getUsername().getUsername(),
                roles
        );
    }
}
