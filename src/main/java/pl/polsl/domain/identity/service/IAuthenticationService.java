package pl.polsl.domain.identity.service;

import pl.polsl.domain.identity.query.GetToken;
import pl.polsl.domain.identity.valueObject.AuthToken;

public interface IAuthenticationService {
    AuthToken getTokenForUser(GetToken query);
}
