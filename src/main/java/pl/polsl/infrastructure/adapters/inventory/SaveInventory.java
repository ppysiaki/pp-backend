package pl.polsl.infrastructure.adapters.inventory;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import pl.polsl.domain.inventory.Inventory;
import pl.polsl.domain.inventory.exception.InventoryIsInUse;
import pl.polsl.domain.inventory.exception.NoSuchInventory;
import pl.polsl.domain.inventory.services.ISaveInventory;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveInventory implements ISaveInventory {
    private final InventoryRepository inventoryRepository;
    private final IMapTo<Inventory, InventoryEntity> iMapTo;


    public SaveInventory(InventoryRepository inventoryRepository, IMapTo<Inventory, InventoryEntity> iMapTo) {
        this.inventoryRepository = inventoryRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(Inventory inventory) {
        inventoryRepository.save(iMapTo.toEntity(inventory, null));
    }

    @Override
    public void update(Integer id, Inventory inventory) throws NoSuchInventory {
        Optional<InventoryEntity> entity = inventoryRepository.findById(id);

        entity.orElseThrow(NoSuchInventory::new);

        inventoryRepository.save(iMapTo.toEntity(inventory, entity.get()));
    }

    @Override
    public void delete(Integer id) throws InventoryIsInUse {
        try {
            inventoryRepository.deleteById(id);
        } catch (DataIntegrityViolationException exception) {
            throw new InventoryIsInUse(exception);
        }
    }
}
