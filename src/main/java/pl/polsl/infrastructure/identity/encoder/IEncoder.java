package pl.polsl.infrastructure.identity.encoder;

public interface IEncoder {
    String encode(String text);

    Boolean match(String rawOldPassword, String encodedRawPassword);
}
