package pl.polsl.domain.inventory.services;

import pl.polsl.domain.inventory.Inventory;

import java.util.List;

public interface IGetInventory {
    Inventory byId(Integer id);

    List<Inventory> getAll();
}
