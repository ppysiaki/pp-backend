package pl.polsl.domain.inventory.exception;

public class InventoryIsInUse extends Exception {
    public InventoryIsInUse(Throwable cause) {
        super("Inventory is assigned to a room or stil has items assigned.", cause);
    }
}
