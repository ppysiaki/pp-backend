package pl.polsl.domain.identity.exception;

public class NoSuchIdentity extends Exception {
    public NoSuchIdentity() {
        super("There is no such Identity in given parameters");
    }
}
