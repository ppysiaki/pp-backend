package pl.polsl.infrastructure.adapters.examples;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ExampleRepository extends CrudRepository<ExampleEntity, Integer> {
    Optional<ExampleEntity> findById(Integer id);
}
