package pl.polsl.application.identity.request;

public class AssignRoleToUserRequest {

    private int id;

    public AssignRoleToUserRequest(int id) {
        this.id = id;
    }

    public int getRoleId() {
        return id;
    }
}
