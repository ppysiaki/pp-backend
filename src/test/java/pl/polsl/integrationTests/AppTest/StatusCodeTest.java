package pl.polsl.integrationTests.AppTest;


import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatusCodeTest {

    private final static Logger LOGGER = Logger.getLogger(StatusCodeTest.class.getName());

    @LocalServerPort
    private int port;

    Helpers helper = new Helpers();

    @Test
    public void testResponseCodes() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        int perfektCode = 200;
        List<String> linkList = new ArrayList<>();

        linkList.add("/tickets");
        linkList.add("/inventories");
        linkList.add("/example");
        linkList.add("/rooms");
        linkList.add("/profile");
        linkList.add("/inventory/1");
        linkList.add("/user/1");
        linkList.add("/user/1/roles/1");
        linkList.add("/user");
        linkList.add("/user/1/roles");
        linkList.add("/inventory/1/inventory-elements");
        linkList.add("/inventory/1/inventory-elements/1");
        linkList.add("/room/1");
        linkList.add("/ticket/1");
        linkList.add("/ticket/1/inventory-elements");

        for (String link : linkList) {

            HttpGet request = new HttpGet(helper.getUrl() + port + link);

            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

            LOGGER.log(Level.INFO, "GET ON: " + request.toString() + " has Status Code " + perfektCode);

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            int responseCode = response.getStatusLine().getStatusCode();
            assertEquals(perfektCode, responseCode, "Response Code is 200 while getting on " + link);
        }
    }
}
