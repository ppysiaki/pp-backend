package pl.polsl.domain.identity.valueObject;

public class UserId {

    private final Integer id;

    public UserId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
