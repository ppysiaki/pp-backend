package pl.polsl.infrastructure.adapters.room;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoomRepository extends CrudRepository<RoomEntity, Integer> {
    Optional<RoomEntity> findById(Integer id);
}
