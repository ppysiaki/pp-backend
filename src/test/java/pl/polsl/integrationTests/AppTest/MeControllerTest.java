package pl.polsl.integrationTests.AppTest;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeControllerTest {

    String link = "/me/changepassword";
    Helpers helper = new Helpers();

    @Test
    public void testChangingPassword() throws IOException {

        String passChanger = "{\"oldPassword\":\"test\",\"newPassword\":\"xxx\",\"user\":\"test\"}";

        String authToken = "Bearer " + helper.createToken();

        HttpPost postRequest = new HttpPost(helper.getUrl() + helper.getPort8080() + link);
        StringEntity entity = new StringEntity(passChanger);

        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        postRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest.setEntity(entity);

        HttpResponse postResponse = HttpClientBuilder.create().build().execute(postRequest);

        assertEquals(200, postResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after POST request");

        String passChanger1 = "{\"oldPassword\":\"xxx\",\"newPassword\":\"test\",\"user\":\"test\"}";

        HttpPost postRequest1 = new HttpPost(helper.getUrl() + helper.getPort8080() + link);
        StringEntity entity1 = new StringEntity(passChanger1);

        postRequest1.setHeader("Accept", "application/json");
        postRequest1.setHeader("Content-type", "application/json");
        postRequest1.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest1.setEntity(entity1);

        HttpResponse postResponse1 = HttpClientBuilder.create().build().execute(postRequest1);

        assertEquals(200, postResponse1.getStatusLine().getStatusCode(),
                "Response Code == 200 after POST request");
    }
}
