package pl.polsl.domain.inventoryelement.services;

import pl.polsl.domain.inventoryelement.InventoryElement;

import java.util.List;

public interface IGetInventoryElement {
    InventoryElement byId(Integer id);

    List<InventoryElement> getAll();

    List<InventoryElement> findAllByInventoryId(Integer id);

    List<InventoryElement> getUnassigned();
}
