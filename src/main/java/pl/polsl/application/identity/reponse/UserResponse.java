package pl.polsl.application.identity.reponse;

import java.util.List;

public class UserResponse {
    private final int id;
    private final String username;
    private final List<RoleResponse> roles;

    public UserResponse(int id, String username, List<RoleResponse> roles) {

        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleResponse> getRoles() {
        return roles;
    }
}
