package pl.polsl.domain.inventory;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class Inventory {

    private Integer id;

    private String symbol;

    @JsonUnwrapped
    private InventoryElementList inventoryElementsList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public InventoryElementList getInventoryElementsList() {
        return inventoryElementsList;
    }

    public void setInventoryElementsList(InventoryElementList inventoryElementsList) {
        this.inventoryElementsList = inventoryElementsList;
    }
}
