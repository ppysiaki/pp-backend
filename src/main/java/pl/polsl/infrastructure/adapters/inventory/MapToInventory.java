package pl.polsl.infrastructure.adapters.inventory;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventory.Inventory;
import pl.polsl.domain.inventory.InventoryElementList;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.inventoryelement.services.IGetInventoryElement;
import pl.polsl.infrastructure.adapters.inventoryElement.InventoryElementEntity;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Component
public class MapToInventory implements IMapTo<Inventory, InventoryEntity> {
    private final IMapTo<InventoryElement, InventoryElementEntity> iMapTo;
    private final IGetInventoryElement getInventoryElement;

    public MapToInventory(IMapTo<InventoryElement, InventoryElementEntity> iMapTo, IGetInventoryElement getInventoryElement) {
        this.iMapTo = iMapTo;
        this.getInventoryElement = getInventoryElement;
    }

    @Override
    public InventoryEntity toEntity(Inventory dto, InventoryEntity entity) {
        if (entity == null) {
            entity = new InventoryEntity();
            entity.setId(dto.getId());
        }

        entity.setSymbol(dto.getSymbol());

        final Set<InventoryElementEntity> inventoryElementEntityList = new HashSet<>();
        if (dto.getInventoryElementsList() != null &&
                dto.getInventoryElementsList().getInventoryElementList() != null &&
                dto.getInventoryElementsList().getInventoryElementList().size() > 0) {
            dto.getInventoryElementsList().getInventoryElementList().forEach(inventoryElement -> inventoryElementEntityList.add(iMapTo.toEntity(inventoryElement, null)));
        }
        entity.setInventoryElementEntities(inventoryElementEntityList);

        return entity;
    }

    @Override
    public Inventory toDto(InventoryEntity entity) {
        Inventory dto = new Inventory();
        dto.setId(entity.getId());
        dto.setSymbol(entity.getSymbol());
        InventoryElementList inventoryElementList = new InventoryElementList();
        inventoryElementList.setInventoryElementList(getInventoryElement.findAllByInventoryId(entity.getId()));
        dto.setInventoryElementsList(inventoryElementList);

        return dto;
    }
}
