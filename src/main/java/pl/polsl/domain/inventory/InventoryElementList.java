package pl.polsl.domain.inventory;

import pl.polsl.domain.inventoryelement.InventoryElement;

import java.util.List;
import java.util.Optional;

public class InventoryElementList {


    private List<InventoryElement> inventoryElementList;

    public Optional<InventoryElement> byId(Integer id) {
        return inventoryElementList.stream().filter(predicate -> predicate.getId().equals(id)).findFirst();
    }

    public List<InventoryElement> getInventoryElementList() {
        return inventoryElementList;
    }

    public void setInventoryElementList(List<InventoryElement> inventoryElementList) {
        this.inventoryElementList = inventoryElementList;
    }


}
