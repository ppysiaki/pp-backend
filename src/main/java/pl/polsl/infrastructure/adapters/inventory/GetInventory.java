package pl.polsl.infrastructure.adapters.inventory;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventory.Inventory;
import pl.polsl.domain.inventory.services.IGetInventory;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetInventory implements IGetInventory {

    private InventoryRepository inventoryRepository;
    private IMapTo<Inventory, InventoryEntity> iMapTo;

    public GetInventory(InventoryRepository inventoryRepository, IMapTo<Inventory, InventoryEntity> iMapTo) {
        this.inventoryRepository = inventoryRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public Inventory byId(Integer id) {
        InventoryEntity entity = this.inventoryRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Inventory> getAll() {
        Iterable<InventoryEntity> inventories = this.inventoryRepository.findAll();
        List<Inventory> output = new ArrayList<>();
        inventories.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}
