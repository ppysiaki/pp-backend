package pl.polsl.integrationTests.AppTest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import pl.polsl.PpApplication;
import pl.polsl.integrationTests.Helpers.Helpers;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PpApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomControllerTest {

    @LocalServerPort
    private int port;


    private String linkToPostGet = "/rooms/";
    private String linkToPutDel = "/room/";
    private String id2 = "2";
    private String postMessage = "{\"inventory\":{\"id\":1,\"symbol\":\"testowy_room\",\"inventoryElementsList\":{\"inventoryElementList\":[{\"id\":1,\"name\":\"lamp_test\",\"broken\":false},{\"id\":2,\"name\":\"table_test\",\"broken\":false},{\"id\":3,\"name\":\"bed_test\",\"broken\":true}]}}}";
    private String postNameToTest = "testowy_room";
    Helpers helper = new Helpers();

    @Test
    public void testGet() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpGet get = new HttpGet(helper.getUrl() + port + linkToPostGet);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        String getContent = EntityUtils.toString(getEntity, "UTF-8");

        assertThat("GET response contains 'PRO'",
                getContent, containsString("PRO"));
    }

    @Test
    public void testPost() throws IOException {

        String authToken = "Bearer " + helper.createToken();
        int start_lenght = helper.getSomeContent(helper.getUrl() + port + linkToPostGet).length();

        HttpPost postRequest = new HttpPost(helper.getUrl() + port + linkToPostGet);
        StringEntity entity = new StringEntity(postMessage);

        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        postRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        postRequest.setEntity(entity);

        HttpResponse postResponse = HttpClientBuilder.create().build().execute(postRequest);

        assertEquals(200, postResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after POST request");

        int second_lenght = helper.getSomeContent(helper.getUrl() + port + linkToPostGet).length();

        assertTrue("POST request added some stuff to table",
                start_lenght < second_lenght);
    }

    @Test
    public void testTheDelete() throws IOException {

        String authToken = "Bearer " + helper.createToken();

        HttpDelete delRequest = new HttpDelete(helper.getUrl() + port + linkToPutDel + id2);
        delRequest.setHeader("Accept", "application/json");
        delRequest.setHeader("Content-type", "application/json");
        delRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse delResponse = HttpClientBuilder.create().build().execute(delRequest);

        assertEquals(200, delResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after DELETE request");

        String getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertFalse("GET response confirms that record were deleted",
                getContent.contains(postNameToTest));
    }

    @Test
    public void testThePut() throws IOException {

        String getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertTrue("GET response contains 'PRO'",
                getContent.contains("PRO"));

        assertFalse("GET response doesn't contains 'UNI'",
                getContent.contains("UNI"));

        String id_to_put = "1";
        String putMessage = "{\"inventory\":{\"id\":2,\"symbol\":\"puted\",\"inventoryElementList\":null}}";

        String authToken = "Bearer " + helper.createToken();
        StringEntity entity = new StringEntity(putMessage);

        HttpPut putRequest = new HttpPut(helper.getUrl() + port + linkToPutDel + id_to_put);
        putRequest.setHeader("Accept", "application/json");
        putRequest.setHeader("Content-type", "application/json");
        putRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        putRequest.setEntity(entity);

        HttpResponse putResponse = HttpClientBuilder.create().build().execute(putRequest);

        assertEquals(200, putResponse.getStatusLine().getStatusCode(),
                "Response Code == 200 after PUT request");

        getContent = helper.getSomeContent(helper.getUrl() + port + linkToPostGet);

        assertThat("GET response contains PUTed message - 'UNI'",
                getContent, containsString("UNI"));


        // Revert changes:
        String revertChanges = "{\"inventory\":{\"id\":1,\"symbol\":\"puted\",\"inventoryElementList\":null}}";
        entity = new StringEntity(revertChanges);

        putRequest = new HttpPut(helper.getUrl() + port + linkToPutDel + id_to_put);
        putRequest.setHeader("Accept", "application/json");
        putRequest.setHeader("Content-type", "application/json");
        putRequest.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        putRequest.setEntity(entity);

        HttpClientBuilder.create().build().execute(putRequest);
    }
}
