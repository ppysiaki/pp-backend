package pl.polsl.integrationTests.Helpers;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Helpers {

    private String url = "http://localhost:";
    private String port8080 = "8080";

    public String createToken() throws IOException {

        String json = "{\"username\":\"test\",\"password\":\"test\"}";

        HttpPost postReq = new HttpPost("http://localhost:8080/user/login");
        StringEntity en = new StringEntity(json);
        postReq.setHeader("Accept", "application/json");
        postReq.setHeader("Content-type", "application/json");
        postReq.setEntity(en);

        HttpResponse postRes = HttpClientBuilder.create().build().execute(postReq);
        HttpEntity token = postRes.getEntity();
        return EntityUtils.toString(token, "UTF-8");
    }

    public String getSomeContent(String uri) throws IOException {

        String authToken = "Bearer " + createToken();

        HttpGet get = new HttpGet(uri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        return EntityUtils.toString(getEntity, "UTF-8");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPort8080() {
        return port8080;
    }

    public void setPort8080(String port8080) {
        this.port8080 = port8080;
    }
}
