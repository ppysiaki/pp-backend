package pl.polsl.domain.ticket.exception;

public class NoSuchTicket extends Exception {
    public NoSuchTicket() {
        super("There is no such Ticket in given parameters");
    }
}
