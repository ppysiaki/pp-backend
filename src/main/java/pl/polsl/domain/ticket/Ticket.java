package pl.polsl.domain.ticket;

import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.room.Room;

public class Ticket {
    private Integer id;

    private Room room;

    private InventoryElement inventoryElement;

    private String description;

    private boolean isDone;

    private String comment;

    private boolean isConfirmed;

    private Integer priority;

    private String notificationDate;

    private String acceptationDate;

    private String executionDate;

    private Integer rate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void assignRoom(Room room) {
        this.room = room;
    }

    public InventoryElement getInventoryElement() {
        return inventoryElement;
    }

    public void setInventoryElement(InventoryElement inventoryElement) {
        this.inventoryElement = inventoryElement;
    }

    public String getDescription() {
        return description;
    }

    public void addDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void markAsDone() {
        isDone = true;
    }

    public void markAsNotDone() {
        isDone = false;
    }

    public String getComment() {
        return comment;
    }

    public void addComment(String comment) {
        this.comment = comment;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void markAsConfirmed() {
        isConfirmed = true;
    }

    public void markAsNotConfirmed() {
        isConfirmed = false;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void addNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getAcceptationDate() {
        return acceptationDate;
    }

    public void addAcceptationDate(String acceptationDate) {
        this.acceptationDate = acceptationDate;
    }

    public String getExecutionDate() {
        return executionDate;
    }

    public void addExecutionDate(String executionDate) {
        this.executionDate = executionDate;
    }

    public Integer getRate() {
        return rate;
    }

    public void addRate(Integer rate) {
        this.rate = rate;
    }
}
