package pl.polsl.infrastructure.adapters.ticket;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventoryelement.services.IGetInventoryElement;
import pl.polsl.domain.room.services.IGetRoom;
import pl.polsl.domain.ticket.Ticket;
import pl.polsl.infrastructure.adapters.inventoryElement.InventoryElementRepository;
import pl.polsl.infrastructure.adapters.room.RoomRepository;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToTicket implements IMapTo<Ticket, TicketEntity> {

    private final RoomRepository roomRepository;

    private final InventoryElementRepository inventoryElementRepository;

    private final IGetRoom getRoom;

    private final IGetInventoryElement getInventoryElement;

    public MapToTicket(RoomRepository roomRepository, InventoryElementRepository inventoryElementRepository, IGetRoom getRoom, IGetInventoryElement getInventoryElement) {
        this.roomRepository = roomRepository;
        this.inventoryElementRepository = inventoryElementRepository;
        this.getRoom = getRoom;
        this.getInventoryElement = getInventoryElement;
    }

    @Override
    public TicketEntity toEntity(Ticket dto, TicketEntity entity) {
        if (null == entity) {
            entity = new TicketEntity();
            entity.setId(dto.getId());
        }

        entity.addComment(dto.getComment());
        entity.addDescription(dto.getDescription());
        entity.setIsDone(dto.isDone());
        if (dto.getRoom() != null && dto.getRoom().getId() != null) {
            entity.assignRoom(roomRepository.findById(dto.getRoom().getId()).orElse(null));
        }
        if (dto.getInventoryElement() != null && dto.getInventoryElement().getId() != null) {
            entity.setInventoryElementEntity(inventoryElementRepository.findById(dto.getInventoryElement().getId()).orElse(null));
        }
        entity.setIsConfirmed(dto.isConfirmed());
        entity.setPriority(dto.getPriority());
        entity.addNotificationDate(dto.getNotificationDate());
        entity.addAcceptationDate(dto.getAcceptationDate());
        entity.addExecutionDate(dto.getExecutionDate());
        entity.addRate(dto.getRate());

        return entity;
    }

    @Override
    public Ticket toDto(TicketEntity entity) {
        Ticket dto = new Ticket();
        dto.setId(entity.getId());
        if (entity.getRoom() != null) {
            dto.assignRoom(getRoom.byId(entity.getRoom().getId()));
        }
        if (entity.getInventoryElementEntity() != null) {
            dto.setInventoryElement(getInventoryElement.byId(entity.getInventoryElementEntity().getId()));
        }
        dto.addComment(entity.getComment());
        dto.addDescription(entity.getDescription());
        dto.setPriority(entity.getPriority());
        dto.addNotificationDate(entity.getNotificationDate());
        dto.addAcceptationDate(entity.getAcceptationDate());
        dto.addExecutionDate(entity.getExecutionDate());
        dto.addRate(entity.getRate());

        if (entity.isDone()) {
            dto.markAsDone();
        } else {
            dto.markAsNotDone();
        }

        if (entity.isConfirmed()) {
            dto.markAsConfirmed();
        } else {
            dto.markAsNotConfirmed();
        }

        return dto;
    }
}
