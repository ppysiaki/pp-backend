package pl.polsl.infrastructure.adapters.inventoryElement;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inventoryelement.InventoryElement;
import pl.polsl.domain.inventoryelement.services.IGetInventoryElement;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetInventoryElement implements IGetInventoryElement {

    private final InventoryElementRepository inventoryElementRepository;
    private final IMapTo<InventoryElement, InventoryElementEntity> iMapTo;

    public GetInventoryElement(InventoryElementRepository inventoryElementRepository, IMapTo<InventoryElement, InventoryElementEntity> iMapTo) {
        this.inventoryElementRepository = inventoryElementRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public InventoryElement byId(Integer id) {
        InventoryElementEntity entity = this.inventoryElementRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<InventoryElement> getAll() {
        Iterable<InventoryElementEntity> inventoryElementEntityList = inventoryElementRepository.findAll();
        List<InventoryElement> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<InventoryElement> findAllByInventoryId(Integer id) {
        Iterable<InventoryElementEntity> inventoryElementEntityList = inventoryElementRepository.findAllByInventoryId(id);
        List<InventoryElement> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<InventoryElement> getUnassigned() {
        Iterable<InventoryElementEntity> inventoryElementEntityList = inventoryElementRepository.findAllByInventoryIdIsNull();
        List<InventoryElement> output = new ArrayList<>();
        inventoryElementEntityList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}
