package pl.polsl.infrastructure.identity.validation.specifications;

import org.springframework.stereotype.Service;
import pl.polsl.infrastructure.identity.repository.IUserRepository;

@Service
public class UniqueUsername {

    private final IUserRepository repository;

    public UniqueUsername(IUserRepository repository) {
        this.repository = repository;
    }

    public boolean isValid(String username) {
        return repository.findAllByUsername(username).size() < 1;
    }
}
