package pl.polsl.infrastructure.adapters.inventory;


import pl.polsl.infrastructure.adapters.inventoryElement.InventoryElementEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "inventory")
public class InventoryEntity {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "symbol")
    private String symbol;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "inventoryId")
    private Set<InventoryElementEntity> inventoryElementEntities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setInventoryElementEntities(Set<InventoryElementEntity> inventoryElementEntities) {
        this.inventoryElementEntities = inventoryElementEntities;
    }
}
