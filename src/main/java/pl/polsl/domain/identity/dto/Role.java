package pl.polsl.domain.identity.dto;

import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.RoleName;

public class Role {

    private final RoleId id;
    private final RoleName name;

    public Role(RoleId id, RoleName name) {
        this.id = id;
        this.name = name;
    }

    public RoleId getRoleId() {
        return id;
    }

    public RoleName getRoleName() {
        return name;
    }
}
