package pl.polsl.domain.inventoryelement;

public class InventoryElement {
    private Integer id;

    private String name;

    private boolean isBroken;

    private Integer inventoryId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public void markAsBroken() {
        isBroken = true;
    }

    public void markAsNotBroken() {
        isBroken = false;
    }

    public void linkToInventory(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }
}
