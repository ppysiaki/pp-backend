package pl.polsl.domain.identity.valueObject;

public class Username {

    private final String username;

    public Username(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return username;
    }

    public String getUsername() {
        return username;
    }
}
