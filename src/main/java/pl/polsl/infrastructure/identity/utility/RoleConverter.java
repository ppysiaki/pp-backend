package pl.polsl.infrastructure.identity.utility;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.RoleName;
import pl.polsl.infrastructure.identity.entity.RoleEntity;

@Component
public class RoleConverter {

    public Role entityToDto(RoleEntity entity) {
        return new Role(
                new RoleId(entity.getId()),
                new RoleName(entity.getName())
        );
    }

    public RoleEntity dtoToEntity(Role role) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(role.getRoleId().getId());
        roleEntity.setName(role.getRoleName().toString());

        return roleEntity;
    }
}
