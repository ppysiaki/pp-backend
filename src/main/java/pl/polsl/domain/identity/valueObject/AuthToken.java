package pl.polsl.domain.identity.valueObject;

public class AuthToken {

    private final String token;

    public AuthToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return token;
    }
}
